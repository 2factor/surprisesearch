#include <iostream>
#include <vector>
#include <cstring>

#include <unistd.h>	
#include "neat.h"
#include "helpers.h"
#include "population.h"
#include "noveltyexp.h"
#include "noveltyset.h"

#include "surprise_math.h"

using namespace std;

void makeDirs(char* s) {
	const string dirname = string(s);
    vector<string> elems;
    split(s, '/', elems);
	stringstream inc_filefolder;
	inc_filefolder << "";
	for(vector<string>::iterator it = elems.begin(); it != elems.end(); ++it) {
		if((*it).empty()){ continue; }
		inc_filefolder << *it << "//";
		char command[250] = {};
		// then remake them
		stringstream ss;
		ss << "mkdir \"" << inc_filefolder.str() << "\"";
		string temp_s = ss.str();
		const char* temp_c = temp_s.c_str();
		system(temp_c);
		
	}
}

void runExperiment(int maze_choice, int optimization_choice, int runs, float ageDecay=1.f, int maxPast=-1, int nearestNeighbors=-1){
	char mazename[100]="hard_maze.txt";
	char filename[100]=".//logs//";
	char settingsname[100]="maze.ne";

	int param=(-1);		// no params for this experiment
	bool useBorders = false;
        
	NEAT::Population *p;
	NEAT::load_neat_params(settingsname,true);

	if(maze_choice==1){
		strcpy(mazename,"medium_maze.txt");
		std::strcat(filename,"medium//");
	} else if(maze_choice==2){
		strcpy(mazename,"hard_maze.txt");
		std::strcat(filename,"hard//");
	} else if(maze_choice==3){
		strcpy(mazename,"mcns_medium_maze.txt");
		std::strcat(filename,"mcns_medium//");
		useBorders = true;
	} else if(maze_choice==4){
		strcpy(mazename,"mcns_hard_maze.txt");
		std::strcat(filename,"mcns_hard//");
		useBorders = true;
	} else if(maze_choice==5){
		strcpy(mazename,"open_medium_maze.txt");
		std::strcat(filename,"open_medium//");
		useBorders = true;
	} else if(maze_choice==6){
		strcpy(mazename,"open_hard_maze.txt");
		std::strcat(filename,"open_hard//");
		useBorders = true;
    } else if(maze_choice==7){
        strcpy(mazename,"very_hard_maze.txt");
        std::strcat(filename,"very_hard//");
    } else if(maze_choice==8){
        strcpy(mazename,"extreme_hard_maze.txt");
        std::strcat(filename,"extremely_hard//");
	} else {
		cout<<"Not an available maze option."<<endl;
	}
	
	if(optimization_choice==1){
		for(int run=1;run<=runs;run++){
			char runFilename[150] = {};
			void (*fn_eval)(Organism*,Environment*);
			sprintf(runFilename,"%s//objective//%d//",filename,run);
			fn_eval = naive_evaluate_fitness;
			makeDirs(runFilename);
			p = maze_fitness_realtime(fn_eval,runFilename,mazename,useBorders,param);
            delete p;
		}
	} else if( optimization_choice==2){
		for(int run=1;run<=runs;run++){
			char runFilename[150] = {};
			void (*nov_eval)(noveltyarchive*,Organism*,Population*,Environment*,bool);
			sprintf(runFilename,"%s//novelty_15//%d//",filename,run);
			nov_eval = naive_evaluate_individual;
			makeDirs(runFilename);
			//p = maze_novelty_realtime(runFilename,mazename,useBorders,param);
			p = maze_novelty_realtime(nov_eval,runFilename,mazename,useBorders,param);
            delete p;
		}
	} else if(optimization_choice == 3) {
		for(int run=1;run<=runs;run++){
			char runFilename[150] = {};
			sprintf(runFilename,"%s//surprise_k_means_%d_neigh_%d//%d//",filename, NEAT::k, NEAT::neigh, run);
			makeDirs(runFilename);
			p = maze_surprise_realtime(runFilename, mazename, useBorders, param);
			cout << "cleaning population" << endl;
			delete p;
		}
	} else {
		cout<<"Not an available optimization option."<<endl;
	}
}

void runExperimentMazeGeneration(int maze_choice, int optimization_choice, int runs, float ageDecay=1.f, int maxPast=-1, int nearestNeighbors=-1) {
	char mazename[100] = "hard_maze.txt";
	char filename[100] = ".//logs//";
	char settingsname[100] = "maze.ne";

	int param = (-1);        // no params for this experiment
	bool useBorders = false;

	NEAT::Population *p;
	NEAT::load_neat_params(settingsname, true);

	std::strcat(filename, "mazes_fixed//");

	const char *maze_complexity[60] = {
			"281_3", "341", "321", "241_2", "305", "344_2", "342_2", "322_3", "283_2", "302_2", "345", "262", "241",
			"301_3", "384", "303_3", "303_2", "202", "263", "303", "363", "281_2", "304", "321_2", "342", "261", "221",
			"241_3", "283", "242", "344", "302", "201_2", "285", "222_2", "223", "222", "282", "243_2", "384_2", "281",
			"262_2", "282_2", "342_3", "322_2", "201", "341_2", "324", "322", "221_2", "301", "261_2", "304_2", "243",
			"261_3", "301_2", "225", "284", "242_2", "341_3"
	};

	if (optimization_choice == 1) {
		for (int run = 1; run <= 60; run++) {

			for (int j = 1; j <= 50; j++) {

				strcpy(mazename, "mazes_fixed/maze_generation");

				strcat(mazename, maze_complexity[run - 1]);

				strcat(mazename, ".txt");


				char runFilename[300] = {};
				void (*fn_eval)(Organism *, Environment *);
				sprintf(runFilename, "%s//objective_%d//%d//", filename, run - 1, j);
				fn_eval = naive_evaluate_fitness;
				makeDirs(runFilename);
				p = maze_fitness_realtime(fn_eval, runFilename, mazename, useBorders, param);
				delete p;
			}
		}
	} else if (optimization_choice == 2) {
		for (int run = 1; run <= 60; run++) {

			for (int j = 1; j <= 50; j++) {

				strcpy(mazename, "mazes_fixed/maze_generation");

				strcat(mazename, maze_complexity[run - 1]);

				strcat(mazename, ".txt");

				char runFilename[300] = {};
				void (*nov_eval)(noveltyarchive *, Organism *, Population *, Environment *, bool);
				sprintf(runFilename, "%s//novelty_15_%d//%d//", filename, run - 1, j);
				nov_eval = naive_evaluate_individual;
				makeDirs(runFilename);
				p = maze_novelty_realtime(nov_eval, runFilename, mazename, useBorders, param);
				delete p;
			}
		}
	} else if (optimization_choice == 3) {
		for (int run = 1; run <= 60; run++) {

			for (int j = 1; j <= 50; j++) {

				strcpy(mazename, "mazes_fixed/maze_generation");

				strcat(mazename, maze_complexity[run - 1]);

				strcat(mazename, ".txt");

				char runFilename[300] = {};

				sprintf(runFilename, "%s//surprise_k_means_%d_neigh_%d_%d//%d//", filename, NEAT::k, NEAT::neigh,
						run - 1, j);

				makeDirs(runFilename);

				p = maze_surprise_realtime(runFilename, mazename, useBorders, param);

				cout << "cleaning population" << endl;
				delete p;
			}
		}
	}
}


int main(int argc, char **argv) {
	char mazename[100]="hard_maze.txt";
	char filename[100]=".//logs//";
	char settingsname[100]="maze.ne";

	NEAT::Population *p;

    int optimization_choice = -1;
    int maze_choice = -1;
    int batchRunMaze = NEAT::maze;

	//***********RANDOM SETUP***************//
	/* Seed the random-number generator with current time so that
		the numbers will be different every time we run.    */
	srand( (unsigned)time( NULL )  + getpid());

    if(argc>4){ optimization_choice=atoi(argv[4]); }
    if (argc>3){ strcpy(mazename,argv[3]); }
    if (argc>2){ strcpy(filename,argv[2]); }

    if(optimization_choice==-1){
        cout << "Please choose an experiment: " << endl;
        cout << "1 - Fitness Run" << endl;
        cout << "2 - Novelty Run" << endl;
        cout << "3 - Surprise K-Means Run" << endl;
        cout << "Number: ";

        cin >> optimization_choice;
    }

    if(optimization_choice < 1 || optimization_choice > 3)
    {
        cout << "ERROR " << endl;
        exit(-1);
    }

    if(maze_choice == -1){
        cout<<"Please choose a mazefile: "<<endl;
        cout<<"1 - Medium Maze" <<endl;
        cout<<"2 - Hard Maze" <<endl;
        cout<<"3 - MCNS Medium Maze" <<endl;
        cout<<"4 - MCNS Hard Maze" <<endl;
        cout<<"5 - Open Medium Maze" <<endl;
        cout<<"6 - Open Hard Maze" <<endl;
        cout<<"7 - Very Hard Maze" <<endl;
        cout<<"8 - Extremely Hard Maze" <<endl;
		cout<<"9 - Automatically Generated" <<endl;
        cout<<"Number: ";
        cin>>maze_choice;
    }

	cout<<"loaded"<<endl;

    if(maze_choice != -1) {
        batchRunMaze = maze_choice;
    }

	if(batchRunMaze >= 0){
		if(maze_choice <= 8)
		{
			runExperiment(batchRunMaze, optimization_choice, 50, 1.f, 20, 15);
		}
		else
		{
			runExperimentMazeGeneration(batchRunMaze, optimization_choice, 50, 1.f, 20, 15);
		}
	}



	return(0);
}