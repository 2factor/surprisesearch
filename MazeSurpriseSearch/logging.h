#ifndef MAZESURPRISESEARCH_LOGGING_H
#define MAZESURPRISESEARCH_LOGGING_H

#include "population.h"

class logging {

public:
    //constructor
    logging();
    ~logging();

    void update_fittest(NEAT::Organism *);

    void update_fittest(NEAT::Population* pop);

    void serialize_fittest(char *fn);

    int generation;

private:
    vector<noveltyitem*> fittest;
};

#endif //MAZESURPRISESEARCH_LOGGING_H
