#ifndef SURPRISE_MATH_H
#define	SURPRISE_MATH_H

#include <vector>
#include <fstream>
#include <map>
#include <unordered_map>
#include <thread>

#include "population.h"
#include "organism.h"
#include "genome.h"

class Environment;

class AbstractSurpriseModel {
    public:
        virtual ~AbstractSurpriseModel() = 0;
        virtual void initLogs(char* output_dir) = 0;
        virtual void assignMaze(Environment* env) = 0;
        virtual void updateValues(NEAT::Population* pop) = 0;
        virtual float evaluateIndividual(NEAT::Organism* org, NEAT::Population* pop = NULL) = 0;
        virtual float evaluateIndividual(std::vector<float> &position) = 0;
};

inline AbstractSurpriseModel::~AbstractSurpriseModel() { }

class KMeansClusteringModel : public AbstractSurpriseModel {
    protected:
        ofstream model_savefile, centroid_savefile, pred_centroid_savefile;
	    vector<vector<double> > prev_centroids;
        int num_cluster;
        vector<vector<double> > predictedcentroids;
        int neigh;
        
    public:
        KMeansClusteringModel(int num_cluster = 1, int neigh = 1);
        ~KMeansClusteringModel();
        void initLogs(char* output_dir);
        void assignMaze(Environment* env);
        void updateValues(NEAT::Population* pop);
        float evaluateIndividual(NEAT::Organism* org, NEAT::Population* pop = NULL);
        float evaluateIndividual(std::vector<float> &position);
};

std::vector<double> predictNextPoint(std::vector<std::vector<double> > &points);
std::vector<float> predictNextPoint(float x1, float y1, float x2, float y2);
std::vector<double> calculateCentroid(std::vector<double> &xcoordinates, std::vector<double> &ycoordinates);
std::vector<float> calculateCentroid(std::vector<std::vector<float> > &points);

#endif	/* SURPRISE_MATH_H */

