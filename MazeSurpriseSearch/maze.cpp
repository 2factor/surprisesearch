#include <vector>
#include <fstream>
#include <iostream>
#include <cmath> 

#include "maze.h"

using namespace std;

Point::Point(float x1,float y1){
	x=x1;
	y=y1;
}

Point::Point(){}

Point::Point(const Point& k){
	x=k.x;
	y=k.y;
}

void Point::fromfile(ifstream& file){
	file >> x;
	file >> y;
}

//determine angle of vector defined by (0,0)->This Point
float Point::angle(){
	if(x==0.0){
		if(y>0.0) return 90.0;
		return 270.0;
	}
	float ang=atan(y/x)/3.1415926*180.0;

	if(isnan(ang)){	cout << "NAN in angle\n"; }
	//quadrant 1 or 4
	if(x>0.0){ return ang; }
	return ang+180.0;             
}

//rotate this point around another point
void Point::rotate(float angle,Point p){
	float rad=angle/180.0*3.1415926;
	x-=p.x;
	y-=p.y;

	float ox=x;
	float oy=y;
	x=cos(rad)*ox-sin(rad)*oy;
	y=sin(rad)*ox+cos(rad)*oy;

	x+=p.x;
	y+=p.y;
}
//distance between this point and another point
float Point::distance(Point b) {
	float dx=b.x-x;
	float dy=b.y-y;
	return sqrt(dx*dx+dy*dy);
}

Line::Line(ifstream& file){
	a.fromfile(file);
	b.fromfile(file);
}
Line::Line(){}
Line::Line(Point k,Point j){
	a.x=k.x;
	a.y=k.y;
	b.x=j.x;
	b.y=j.y;
}
Line::Line(float x1,float y1,float x2,float y2){
	a.x=x1;
	a.y=y1;
	b.x=x2;
	b.y=y2;
}
Line::Line(Point k,float x2,float y2){
	a.x=k.x;
	a.y=k.y;
	b.x=x2;
	b.y=y2;
}
Line::Line(float x1,float y1,Point j){
	a.x=x1;
	a.y=y1;
	b.x=j.x;
	b.y=j.y;
}

//midpoint of the line segment
Point Line::midpoint(){
	Point newpoint;
	newpoint.x=(a.x+b.x)/2.0;
	newpoint.y=(a.y+b.y)/2.0;
	return newpoint;
}
 
//return point of intersection between two line segments if it exists 
Point Line::intersection(Line L,bool &found){      
	Point pt(0.0,0.0);
	Point A(a);
	Point B(b);
	Point C(L.a);
	Point D(L.b);


	float rTop = (A.y-C.y)*(D.x-C.x)-(A.x-C.x)*(D.y-C.y);
	float rBot = (B.x-A.x)*(D.y-C.y)-(B.y-A.y)*(D.x-C.x);

	float sTop = (A.y-C.y)*(B.x-A.x)-(A.x-C.x)*(B.y-A.y);
	float sBot = (B.x-A.x)*(D.y-C.y)-(B.y-A.y)*(D.x-C.x);

	if ( (rBot == 0) || (sBot == 0)){
		//lines are parallel
		found = false;
		return pt;
	}

	float r = rTop/rBot;
	float s = sTop/sBot;

	if( (r > 0) && (r < 1) && (s > 0) && (s < 1) ){
		pt.x = A.x + r * (B.x - A.x);
		pt.y = A.y + r * (B.y - A.y);

		found=true;
		return pt;
	} else {

		found=false;
		return pt;
	}

}
    
//distance between line segment and point
float Line::distance(Point n) {
	float utop = (n.x-a.x)*(b.x-a.x)+(n.y-a.y)*(b.y-a.y);
	float ubot = a.distance(b);
	ubot*=ubot;
	if(ubot==0.0){
		cout << "Ubot zero?" << endl;
		return 0.0;
	}
	float u = utop/ubot;

	if(u<0 || u>1){
		float d1=a.distance(n);
		float d2=b.distance(n);
		if(d1<d2) return d1;
		return d2;
	}
	Point p;
	p.x=a.x+u*(b.x-a.x);
	p.y=a.y+u*(b.y-a.y);
	return p.distance(n);
}

//line segment length
float Line::length() {
	return a.distance(b);
}
/*
Character::Character() {
	heading=0.0f;
	speed=0.0f;
	ang_vel=0.0f;
}
*/
Character::Character() {
	heading=0.0f;
	speed=0.0f;
	ang_vel=0.0f;
	radius=8.0f;
	rangefinder_range=100.0f;

	//define the range finder sensors			
	rangeFinderAngles.push_back(-90.0f);
	rangeFinderAngles.push_back(-45.0f);
	rangeFinderAngles.push_back(0.0f);
	rangeFinderAngles.push_back(45.0f);
	rangeFinderAngles.push_back(90.0f);
	rangeFinderAngles.push_back(-180.0f);

	//define the radar sensors
	radarAngles1.push_back(315.0);
	radarAngles2.push_back(405.0);

	radarAngles1.push_back(45.0);
	radarAngles2.push_back(135.0);

	radarAngles1.push_back(135.0);
	radarAngles2.push_back(225.0);

	radarAngles1.push_back(225.0);
	radarAngles2.push_back(315.0);

	for(int i=0;i<(int)rangeFinderAngles.size();i++)
		rangeFinders.push_back(0.0);

	for(int i=0;i<(int)radarAngles1.size();i++)
		radar.push_back(0.0);
}
/*
Environment::Environment(const char* filename){
	ifstream inpfile(filename);
	hero.location.fromfile(inpfile);
	end.fromfile(inpfile);
	while(!inpfile.eof()){
		Line* x=new Line(inpfile); 
		lines.push_back(x);
	}
}
*/
Environment::Environment(const Environment &e){
	if(e.borders){ borders = e.borders; }
	hero.location = e.hero.location;
	hero.heading = e.hero.heading;
	hero.speed=e.hero.speed;
	hero.ang_vel=e.hero.ang_vel;
	end=e.end;
	start=e.start;
	for(int i=0;i<(int)e.lines.size();i++)
	{
		Line* x=new Line(*(e.lines[i]));
		lines.push_back(x);
	}
	update_rangefinders(hero);
	update_radar(hero);
	reachgoal=e.reachgoal;
}
//initialize environment from maze file
Environment::Environment(const char* filename, bool useBorders){
	ifstream inpfile(filename);
	if(useBorders){
		borders=new Line(inpfile); 
	} else {
		borders = NULL;
	}
	int num_lines;
	inpfile >> num_lines; //read in how many line segments
	start.fromfile(inpfile); //read initial location
	hero.location.x = start.x;
	hero.location.y = start.y;
	inpfile >> hero.heading; //read initial heading
	end.fromfile(inpfile); //read goal location
	reachgoal=0;
	//read in line segments
	for(int i=0;i<num_lines;i++)
	{
		Line* x=new Line(inpfile); 
		lines.push_back(x);
	}
	//update sensors
	update_rangefinders(hero);
	update_radar(hero);
}

//debug function
void Environment::display(){
	cout << "Hero: " << hero.location.x << " " << hero.location.y << endl;
	cout << "StartPoint: " << start.x << " " << start.y << endl;
	cout << "EndPoint: " << end.x << " " << end.y << endl;
	cout << "Lines:" << endl;
	for(int i=0;i<(int)lines.size();i++)
	{
	cout << lines[i]->a.x << " " << lines[i]->a.y << " " << lines[i]->b.x << " " << lines[i]->b.y << endl;
	}
}

//used for fitness calculations
float Environment::distance_to_target(){
	float dist=hero.location.distance(end);
	if(isnan(dist)){
		cout << "NAN Distance error..." << endl;
		return 500.0;
	}
	if(dist<5.0) reachgoal=1; //if within 5 units, success!
	return dist;
}	

//create neural net inputs from sensors
void Environment::generate_neural_inputs(double* inputs){			
	//bias
	inputs[0]=(1.0);

	//rangefinders
	int i;
	for(i=0;i<(int)hero.rangeFinders.size();i++){
		inputs[1+i]=(hero.rangeFinders[i]/hero.rangefinder_range);
		if(isnan(inputs[1+i]))
			cout << "NAN in inputs" << endl;
	}

	//radar
	for(int j=0;j<(int)hero.radar.size();j++){
		inputs[i+j]=(hero.radar[j]);
		if(isnan(inputs[i+j]))
			cout << "NAN in inputs" << endl;
	}

	return;
}

//transform neural net outputs into angular velocity and speed
void Environment::interpret_outputs(float o1,float o2){
	if(isnan(o1) || isnan(o2)){	cout << "OUTPUT ISNAN" << endl;	}

	hero.ang_vel+=(o1-0.5)*1.0;
	hero.speed+=(o2-0.5)*1.0;

	//constraints of speed & angular velocity
	if(hero.speed>3.0) hero.speed=3.0;
	if(hero.speed<-3.0) hero.speed=(-3.0);
	if(hero.ang_vel>3.0) hero.ang_vel=3.0;
	if(hero.ang_vel<-3.0) hero.ang_vel=(-3.0);
}

//run a time step of the simulation
void Environment::Update(){
if (reachgoal)
	return;
	float vx=cos(hero.heading/180.0*3.1415926)*hero.speed;
	float vy=sin(hero.heading/180.0*3.1415926)*hero.speed;
	if(isnan(vx))
		cout << "VX NAN" << endl;

	hero.heading+=hero.ang_vel;
	if(isnan(hero.ang_vel))
		cout << "HERO ANG VEL NAN" << endl;

	if(hero.heading>360) hero.heading-=360;
	if(hero.heading<0) hero.heading+=360;

	Point newloc;
	newloc.x=vx+hero.location.x;
	newloc.y=vy+hero.location.y;

	//collision detection
	if(!collide_lines(newloc,hero.radius))
	{
		hero.location.x=newloc.x;
		hero.location.y=newloc.y;
	}

	update_rangefinders(hero);
	update_radar(hero);
}

//see if navigator is outside the borders
bool Environment::outOfBounds(float loc_x, float loc_y) {
	if(!borders){ return false; }
	if(loc_x<borders->a.x){ return true; }
	if(loc_y<borders->a.y){ return true; }
	if(loc_x>borders->b.x){ return true; }
	if(loc_y>borders->b.y){ return true; }
	return false;
}
/*
//see if navigator is outside the borders
bool Environment::outOfBounds(Point loc) {
	if(!borders){ return false; }
	if(loc.x<borders->a.x){ return true; }
	if(loc.y<borders->a.y){ return true; }
	if(loc.x>borders->b.x){ return true; }
	if(loc.y>borders->b.y){ return true; }
	return false;
}
//see if navigator is outside the borders (encoded in the way the data point is stored on the maze runner
bool Environment::outOfBounds(vector<float> loc) {
	if(!borders){ return false; }
	if(loc.at(0)<borders->a.x){ return true; }
	if(loc.at(1)<borders->a.y){ return true; }
	if(loc.at(0)>borders->b.x){ return true; }
	if(loc.at(1)>borders->b.y){ return true; }
	return false;
}
*/
//see distance of navigator from the closest border
float Environment::distFromBounds(float loc_x, float loc_y) {
	if(!borders){ return false; }
	if(!outOfBounds(loc_x,loc_y)){ return 0; }
	float result = 0;
	float dist_from_left = borders->a.x-loc_x;
	float dist_from_top = borders->a.y-loc_y;
	float dist_from_right = loc_x-borders->b.x;
	float dist_from_bottom = loc_y-borders->b.y;
	if(dist_from_left>0){
		if(dist_from_top>0){ 
			//result=fminf(dist_from_left,dist_from_top);
			result = sqrt(dist_from_left*dist_from_left+dist_from_top*dist_from_top);
		} else if(dist_from_bottom>0){ 
			//result=fminf(dist_from_left,dist_from_bottom);
			result = sqrt(dist_from_left*dist_from_left+dist_from_bottom*dist_from_bottom);
		} else {
			result = dist_from_left;
		}
	} else if(dist_from_right>0){
		if(dist_from_top>0){ 
			//result=fminf(dist_from_right,dist_from_top);
			result = sqrt(dist_from_right*dist_from_right+dist_from_top*dist_from_top);
		} else if(dist_from_bottom>0){ 
			//result=fminf(dist_from_right,dist_from_bottom);
			result = sqrt(dist_from_right*dist_from_right+dist_from_bottom*dist_from_bottom);
		} else {
			result = dist_from_right;
		}
	}
	return result;
}

//see if navigator has hit anything
bool Environment::collide_lines(Point loc,float rad){
	for(int i=0;i<(int)lines.size();i++){
		if(lines[i]->distance(loc)<rad)
			return true;
	}
	return false;
}

//rangefinder sensors
void Environment::update_rangefinders(Character& h){
	//iterate through each sensor
	for(int i=0;i<(int)h.rangeFinderAngles.size();i++) {
		float rad=h.rangeFinderAngles[i]/180.0*3.1415926; //radians...

		//project a point from the hero's location outwards
		Point proj_point(h.location.x+cos(rad)*h.rangefinder_range,
						 h.location.y+sin(rad)*h.rangefinder_range);

		//rotate the project point by the hero's heading
		proj_point.rotate(h.heading,h.location);

		//create a line segment from the hero's location to projected
		Line projected_line(h.location,proj_point);

		float range=h.rangefinder_range; //set range to max by default

		//now test against the environment to see if we hit anything
		for(int j=0;j<(int)lines.size();j++)
		{
			bool found=false;
			Point intersection=lines[j]->intersection(projected_line,found);
			if(found)
			{
				//if so, then update the range to the distance
				float found_range = intersection.distance(h.location);

				//we want the closest intersection
				if(found_range<range)
					range=found_range; 
			}
		}
		if(isnan(range))
			cout << "RANGE NAN" << endl;
		h.rangeFinders[i]=range;
	}
}

//radar sensors
void Environment::update_radar(Character& h){
	Point target=end; 

	//rotate goal with respect to heading of navigator
	target.rotate(-h.heading,h.location);

	//translate with respect to location of navigator
	target.x-=h.location.x;
	target.y-=h.location.y;

	//what angle is the vector between target & navigator
	float angle=target.angle();

	//fire the appropriate radar sensor
	for(int i=0;i<(int)h.radarAngles1.size();i++)
	{
		h.radar[i]=0.0;

		if(angle>=h.radarAngles1[i] && angle<h.radarAngles2[i])
			h.radar[i]=1.0;

		if(angle+360.0>=h.radarAngles1[i] && angle+360.0<h.radarAngles2[i])
			h.radar[i]=1.0;  
	}
}

Environment::~Environment()
{
	//clean up lines!
	for(int i=0;i<(int)lines.size();i++)
		delete lines[i];
}


