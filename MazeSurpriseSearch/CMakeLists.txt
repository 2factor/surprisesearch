cmake_minimum_required(VERSION 3.5)
project(MazeSurpriseSearch)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++0x")

set(SOURCE_FILES
    datarec.h
    gene.cpp
    gene.h
    genome.cpp
    genome.h
    helpers.h
    histogram.h
    innovation.cpp
    innovation.h
    link.cpp
    link.h
    linreg.cpp
    linreg.h
    Makefile
    Matrix.cpp
    Matrix.h
    maze.cpp
    maze.h
    neat.cpp
    neat.h
    neatmain.cpp
    neatmain.h
    network.cpp
    network.h
    nnode.cpp
    nnode.h
    noveltyexp.cpp
    noveltyexp.h
    noveltyitem.h
    noveltyset.cpp
    noveltyset.h
    organism.cpp
    organism.h
    population.cpp
    population.h
    species.cpp
    species.h
    surprise_math.cpp
    surprise_math.h
        trait.cpp
    trait.h
    Vector.h
    logging.h
    logging.cpp)

add_executable(MazeSurpriseSearch ${SOURCE_FILES})