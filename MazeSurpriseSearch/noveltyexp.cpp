#include "noveltyexp.h"
#include "noveltyset.h"
#include "logging.h"

#include "datarec.h"
#include "maze.h"
#include "helpers.h"
#include "histogram.h"

#include "surprise_math.h"

//#define DEBUG_OUTPUT 1
#include <algorithm>
#include <vector>
#include <cstring>
#include <iostream>
#include <fstream>
#include <math.h>
#include <ctime>	

using namespace std;

static char output_dir[80]="";
static Environment* env;
static int param=-1;

static bool compactLogs = true;


bool printDiscoveryStats(bool firstflag, int offspring_count, double elapsed_secs){
	char fname[100];
	
	// find the right output_dir: one-up
	vector<string> elems;
    split(output_dir, '/', elems);
	stringstream oneup_dir;
	oneup_dir << "";
	int down_count = elems.size();
	for(vector<string>::iterator it = elems.begin(); it != elems.end(); ++it) {
		down_count--;
		if((*it).empty()){ continue; }
		if(down_count>1){ oneup_dir << *it << "//"; }
	}
	const std::string oneup_dir_str = oneup_dir.str();
	const char* oneup_dir_cstr = oneup_dir_str.c_str();
	cout << "saving overview logs at " << oneup_dir_str << endl;

	sprintf(fname,"%sdiscovery",oneup_dir_cstr);
	std::ofstream outFile(fname, ios::out | ios::app ); 
	if (!outFile) { return false; }
	if(firstflag){ outFile << "1 "; } else { outFile << "0 "; }
	outFile << offspring_count/NEAT::pop_size << " ";
	outFile << offspring_count << " ";
	outFile << elapsed_secs;
	outFile << endl;
	outFile.close();
}

//novelty metric for maze simulation
float maze_novelty_metric(noveltyitem* x,noveltyitem* y){
	float diff = 0.0;
	for(int k=0;k<(int)x->data.size();k++) {
		diff+=hist_diff(x->data[k],y->data[k]);
	}
	return diff;
}

//fitness simulation of maze navigation
Population *maze_fitness_realtime(void(*fn_eval)(Organism*,Environment*),char* outputdir,const char *mazefile,bool useBorders,int par) {
	Population *pop;
	Genome *start_genome;
	char curword[20];
	int id;
	
	//create new maze environment
	env=new Environment(mazefile,useBorders);
	if(outputdir!=NULL) strcpy(output_dir,outputdir);
	param=par;

	//clear a bunch of appendable logs
	char fName[100];
	sprintf(fName,"%sdistance",output_dir);
	std::ofstream *outFile = new std::ofstream(fName,std::ios::out );
	sprintf(fName,"%sfeasible",output_dir);
        std::ofstream *outFile2 = new std::ofstream(fName,std::ios::out );
	outFile2 = new std::ofstream(fName,std::ios::out );
	
	//starter gene file
	ifstream iFile("mazestartgenes",ios::in);

	cout<<"START MAZE NAVIGATOR FITNESS REAL-TIME EVOLUTION VALIDATION"<<endl;

	cout<<"Reading in the start genome"<<endl;
	//Read in the start Genome
	iFile>>curword;
	iFile>>id;
	cout<<"Reading in Genome id "<<id<<endl;
	start_genome=new Genome(id,iFile);
	iFile.close();

	cout<<"Start Genome: "<<start_genome<<endl;

	//Spawn the Population from starter gene
	cout<<"Spawning Population off Genome"<<endl;
	pop=new Population(start_genome,NEAT::pop_size);

	cout<<"Verifying Spawned Pop"<<endl;
	pop->verify();

	//Start the evolution loop using rtNEAT method calls 
	maze_fitness_realtime_loop(fn_eval,pop);

	cout<<"Experiment done, cleaning up"<<endl;
    //clean up
	delete env;
        
        delete outFile;
        delete outFile2;
    
	cout<<"Returning Pop"<<endl;
    return pop;
}

//actual rtNEAT loop for fitness run of maze navigation
int maze_fitness_realtime_loop(void(*fn_eval)(Organism*,Environment*),Population *pop) {
	clock_t begin_time = clock();
	
	bool firstflag=false; //indicates whether maze has been solved yet
	int indiv_counter=0;
	vector<Organism*>::iterator curorg;
	vector<Species*>::iterator curspecies;
	vector<Species*>::iterator curspec; //used in printing out debug info                                                         
	vector<Species*> sorted_species;  //Species sorted by max fit org in Species                                                  

	data_rec Record; //holds run information
	int count=0;
	int pause;

	//Real-time evolution variables                                                                                             
	int offspring_count;
	Organism *new_org;

	//We try to keep the number of species constant at this number                                                    
	int num_species_target=NEAT::pop_size/NEAT::target_species_size;

	//This is where we determine the frequency of compatibility threshold adjustment
	int compat_adjust_frequency = NEAT::pop_size/20;
	if (compat_adjust_frequency < 1){ 
		compat_adjust_frequency = 1; 
	}
        
        //NEAT::print_every = 1;

	//Initially, we evaluate the whole population                                                                               
	//Evaluate each organism on a test                                                                                          
	for(curorg=(pop->organisms).begin();curorg!=(pop->organisms).end();++curorg) {
		//shouldn't happen                                                                                                        
		if (((*curorg)->gnome)==0) {
			cout<<"ERROR EMPTY GEMOME!"<<endl;
			cin>>pause;
		}
		//map the novelty point to each individual (this runs the maze simulation)
		(*curorg)->noveltypoint = maze_novelty_map((*curorg));
		(*curorg)->noveltypoint->indiv_number = indiv_counter;
		indiv_counter++;
		//(*curorg)->fitness = (*curorg)->noveltypoint->fitness;
		// the entire evaluation is happening here: no function pointer is needed?
		fn_eval((*curorg),env);
	}

	//Get ready for real-time loop
	//Rank all the organisms from best to worst in each species
	pop->rank_within_species();                                                                            

	//Assign each species an average fitness 
	//This average must be kept up-to-date by rtNEAT in order to select species probabailistically for reproduction
	pop->estimate_all_averages();

	cout <<"Entering real time loop..." << endl;

	//Now create offspring one at a time, testing each offspring,                                                               
	// and replacing the worst with the new offspring if its better

	//run for 2000 generations (250*2000 = 500,000 evaluations)
	for(offspring_count=0;offspring_count<NEAT::pop_size*NEAT::generations;offspring_count++) {
		if(offspring_count % (NEAT::pop_size*NEAT::print_every) == 0 ) {	// every "print-every" generation
			cout << "Generation: " << offspring_count/NEAT::pop_size << endl;
			//cout << offspring_count << endl;
			char fname[100];
			if(!compactLogs){
				sprintf(fname,"%sfit_rtgen_%d",output_dir,offspring_count/NEAT::pop_size);
				pop->print_to_file_by_species(fname);
			}
			sprintf(fname,"%spos_%d",output_dir,offspring_count/NEAT::pop_size);
			pop->print_positions_to_file(fname);
		}
		if(offspring_count % (NEAT::pop_size) == 0 ) {	// every generation
			char fname[100];
			sprintf(fname,"%sdistance",output_dir);
			pop->print_distance_to_progress_file(env->end,fname);
			sprintf(fname,"%sfeasible",output_dir);
			pop->print_feasible_to_progress_file(env,fname);
		}
		
		//Every pop_size reproductions, adjust the compat_thresh to better match the num_species_targer
		//and reassign the population to new species                                              
		if (offspring_count % compat_adjust_frequency == 0) {
			count++;
			#ifdef DEBUG_OUTPUT
			cout << "Adjusting..." << endl;
			#endif


			int num_species = pop->species.size();
			double compat_mod=0.1;  //Modify compat thresh to control speciation                                                     

			// This tinkers with the compatibility threshold 
			if (num_species < num_species_target) {
				  NEAT::compat_threshold -= compat_mod;
			} else if (num_species > num_species_target){
				NEAT::compat_threshold += compat_mod;
			}

			if (NEAT::compat_threshold < 0.3){
				NEAT::compat_threshold = 0.3;
			}
			#ifdef DEBUG_OUTPUT
			cout<<"compat_thresh = "<<NEAT::compat_threshold<<endl;
			#endif
			//Go through entire population, reassigning organisms to new species                                                  
			for (curorg = (pop->organisms).begin(); curorg != pop->organisms.end(); ++curorg) {
				pop->reassign_species(*curorg);
			}
		}


		//For printing only
		#ifdef DEBUG_OUTPUT
		for(curspec=(pop->species).begin();curspec!=(pop->species).end();curspec++) {
		  cout<<"Species "<<(*curspec)->id<<" size"<<(*curspec)->organisms.size()<<" average= "<<(*curspec)->average_est<<endl;
		}

		cout<<"Pop size: "<<pop->organisms.size()<<endl;
		#endif
		//Here we call two rtNEAT calls: 
		//1) choose_parent_species() decides which species should produce the next offspring
		//2) reproduce_one(...) creates a single offspring fromt the chosen species
		new_org=(pop->choose_parent_species())->reproduce_one(offspring_count,pop,pop->species);

		#ifdef DEBUG_OUTPUT
		cout<<"Evaluating new baby: "<<endl;
		#endif

		//create record for new individual
		data_record *newrec = new data_record();
		newrec->indiv_number=indiv_counter;
		//evaluate new individual
		new_org->noveltypoint = maze_novelty_map(new_org,newrec);
		new_org->noveltypoint->indiv_number = indiv_counter;
		//grab novelty
		newrec->ToRec[RECSIZE-2]=new_org->noveltypoint->novelty;
		//set organism's fitness 
		fn_eval((*curorg),env);
		indiv_counter++;

		#ifdef DEBUG_OUTPUT
		cout << "Fitness: " << new_org->fitness << endl;
		cout << "Novelty: " << new_org->noveltypoint->novelty << endl;
		cout << "RFit: " << new_org->noveltypoint->fitness << endl;
		#endif

		//add record of new individual to storage
		Record.add_new(newrec);

		//Now we reestimate the baby's species' fitness
		new_org->species->estimate_average();

		//Remove the worst organism                                                                                               
		pop->remove_worst();

		//store first solution organism
		if(!firstflag && newrec->ToRec[3]>0.0) {
			firstflag=true;
			char filename[100];
			sprintf(filename,"%sfit_rtgen_first",output_dir);
			pop->print_to_file_by_species(filename);
			cout << "Maze solved by indiv# " << indiv_counter << endl;		
			break;
		}
	}
	clock_t end_time = clock();
	double elapsed_secs = double(end_time - begin_time) / CLOCKS_PER_SEC;

	//write out run information, archive, and final generation
	cout << "COMPLETED...";
	char fname[150];
	
	if(!compactLogs){
		sprintf(fname,"%srecord.dat",output_dir);
		Record.serialize(fname);
	}
	sprintf(fname,"%srtgen_final",output_dir);
	pop->print_to_file_by_species(fname);
	
	sprintf(fname,"%spos_final",output_dir);
	pop->print_positions_to_file(fname);

	printDiscoveryStats(firstflag,offspring_count,elapsed_secs);
	
	cout << "returning"<< endl;
	
	/*
	//finish up, write out the record and the final generation
	cout << "COMPLETED..." << endl;
	char filename[30];
	sprintf(filename,"%srecord.dat",output_dir);
	Record.serialize(filename);
	sprintf(filename,"%sfit_rtgen_final",output_dir);
	pop->print_to_file_by_species(filename);
	*/
	return 0;
}

//novelty maze navigation run
Population *maze_novelty_realtime(void(*nov_eval)(noveltyarchive*,Organism*,Population*,Environment*,bool),char* outputdir,const char* mazefile,bool useBorders,int par) {
    Population *pop;
    Genome *start_genome;
    char curword[20];
    int id;

	//create new maze environment
	env=new Environment(mazefile,useBorders);
	param=par;
	if(outputdir!=NULL) strcpy(output_dir,outputdir);
		
	//clear a bunch of appendable logs
	char fName[100];
	sprintf(fName,"%sdistance",output_dir);
	std::ofstream *outFile = new std::ofstream(fName,std::ios::out );
	sprintf(fName,"%sfeasible",output_dir);
	outFile = new std::ofstream(fName,std::ios::out );
	
	//starter genes file
    ifstream iFile("mazestartgenes",ios::in);
	
    cout<<"START MAZE NAVIGATOR NOVELTY REAL-TIME EVOLUTION VALIDATION"<<endl;

    cout<<"Reading in the start genome"<<endl;
    //Read in the start Genome
    iFile>>curword;
    iFile>>id;
    cout<<"Reading in Genome id "<<id<<endl;
    start_genome=new Genome(id,iFile);
    iFile.close();

    cout<<"Start Genome: "<<start_genome<<endl;

    //Spawn the Population from starter gene
    cout<<"Spawning Population off Genome"<<endl;
    pop=new Population(start_genome,NEAT::pop_size);
      
    cout<<"Verifying Spawned Pop"<<endl;
    pop->verify();
    
    //Start the evolution loop using rtNEAT method calls 
    maze_novelty_realtime_loop(nov_eval,pop);

	cout<<"Experiment done, cleaning up"<<endl;
    //clean up
	delete env;
    
	cout<<"Returning Pop"<<endl;
    return pop;
}

//actual rtNEAT loop for novelty maze navigation runs
//int maze_novelty_realtime_loop(Population *pop) {	// BACKUP FOR SANITY
int maze_novelty_realtime_loop(void(*nov_eval)(noveltyarchive*,Organism*,Population*,Environment*,bool),Population *pop) {
	clock_t begin_time = clock();
	
	bool firstflag=false; //indicates whether the maze has been solved yet

	vector<Organism*>::iterator curorg;
	vector<Species*>::iterator curspecies;
	vector<Species*>::iterator curspec; //used in printing out debug info                                                         

	vector<Species*> sorted_species;  //Species sorted by max fit org in Species 

	float archive_thresh=6.0; //initial novelty threshold

	//archive of novel behaviors
	noveltyarchive archive(archive_thresh,*maze_novelty_metric,false);
        
        //NEAT::print_every = 1;

	data_rec Record; //stores run information

	int count=0;
	int pause;

	//Real-time evolution variables                                                                                             
	int offspring_count;
	Organism *new_org;

	//We try to keep the number of species constant at this number                                                    
	int num_species_target=NEAT::pop_size/NEAT::target_species_size;
  
	//This is where we determine the frequency of compatibility threshold adjustment
	int compat_adjust_frequency = NEAT::pop_size/20;
	if (compat_adjust_frequency < 1){ 
		compat_adjust_frequency = 1; 
	}

	//Initially, we evaluate the whole population                                                                               
	//Evaluate each organism on a test                   
	int indiv_counter=0;  
	for(curorg=(pop->organisms).begin();curorg!=(pop->organisms).end();++curorg) {

		//shouldn't happen                                                                                                        
		if (((*curorg)->gnome)==0) {
			cout<<"ERROR EMPTY GEMOME!"<<endl;
			cin>>pause;
		}

		//evaluate each individual
		(*curorg)->noveltypoint = maze_novelty_map((*curorg));
		(*curorg)->noveltypoint->indiv_number=indiv_counter;
		indiv_counter++;
	}

	//assign fitness scores based on novelty
	archive.evaluate_population(pop,env,nov_eval,true);
	//add to archive
	archive.evaluate_population(pop,env,nov_eval,false);

	//Get ready for real-time loop
	//Rank all the organisms from best to worst in each species
	pop->rank_within_species();                                                                            

	//Assign each species an average fitness 
	//This average must be kept up-to-date by rtNEAT in order to select species probabailistically for reproduction
	pop->estimate_all_averages();

	cout <<"Entering real time loop..." << endl;

	//Now create offspring one at a time, testing each offspring,                                                               
	// and replacing the worst with the new offspring if its better
	for (offspring_count=0;offspring_count<NEAT::pop_size*NEAT::generations;offspring_count++) {
		//only continue past generation 1000 if not yet solved
		if(offspring_count>=pop_size*1000 && firstflag){
			break;
		}

		//end of generation
		if(offspring_count % (NEAT::pop_size*1) == 0) {
			archive.end_of_gen_steady(pop);
			//archive.add_randomly(pop);
			archive.evaluate_population(pop,env,nov_eval,false);
			//cout << "ARCHIVE SIZE:" << archive.get_set_size() << endl;
		}

		//write out current generation and fittest individuals
		if( offspring_count % (NEAT::pop_size*NEAT::print_every) == 0 ) {
			cout << "Generation: " << offspring_count/NEAT::pop_size << endl;
			char fname[100];

			sprintf(fname,"%sfittest_%d",output_dir,offspring_count/NEAT::pop_size);
			archive.serialize_fittest(fname);

			//cout << "saving fittest to " << fname << endl;
			
			if(!compactLogs){
				sprintf(fname,"%srtgen_%d",output_dir,offspring_count/NEAT::pop_size);
				pop->print_to_file_by_species(fname);
			}
			sprintf(fname,"%spos_%d",output_dir,offspring_count/NEAT::pop_size);
			pop->print_positions_to_file(fname);
		}
		if(offspring_count % (NEAT::pop_size) == 0 ) {	// every generation
			char fname[100];
			sprintf(fname,"%sdistance",output_dir);
			pop->print_distance_to_progress_file(env->end,fname);
			sprintf(fname,"%sfeasible",output_dir);
			pop->print_feasible_to_progress_file(env,fname);
		}
		
		//Every pop_size reproductions, adjust the compat_thresh to better match the num_species_targer
		//and reassign the population to new species                                              
		if (offspring_count % compat_adjust_frequency == 0) {
			count++;
			#ifdef DEBUG_OUTPUT
			cout << "Adjusting..." << endl;
			#endif

			//update fittest individual list		
			archive.update_fittest(pop);
			//refresh generation's novelty scores
			archive.evaluate_population(pop,env,nov_eval,true);

			int num_species = pop->species.size();
			double compat_mod=0.1;  //Modify compat thresh to control speciation                                                     

			// This tinkers with the compatibility threshold 
			if (num_species < num_species_target) {
				NEAT::compat_threshold -= compat_mod;
			}
			else if (num_species > num_species_target){
				NEAT::compat_threshold += compat_mod;
			}
			
			if (NEAT::compat_threshold < 0.3){
				NEAT::compat_threshold = 0.3;
			}
			
			#ifdef DEBUG_OUTPUT
			cout<<"compat_thresh = "<<NEAT::compat_threshold<<endl;
			#endif

			//Go through entire population, reassigning organisms to new species                                                  
			for (curorg = (pop->organisms).begin(); curorg != pop->organisms.end(); ++curorg) {
				pop->reassign_species(*curorg);
			}
		}

		//For printing only
		#ifdef DEBUG_OUTPUT
		for(curspec=(pop->species).begin();curspec!=(pop->species).end();curspec++) {
			cout<<"Species "<<(*curspec)->id<<" size"<<(*curspec)->organisms.size()<<" average= "<<(*curspec)->average_est<<endl;
		}

		cout<<"Pop size: "<<pop->organisms.size()<<endl;
		#endif

		//Here we call two rtNEAT calls: 
		//1) choose_parent_species() decides which species should produce the next offspring
		//2) reproduce_one(...) creates a single offspring fromt the chosen species
		new_org=(pop->choose_parent_species())->reproduce_one(offspring_count,pop,pop->species);

		//Now we evaluate the new individual
		//Note that in a true real-time simulation, evaluation would be happening to all individuals at all times.
		//That is, this call would not appear here in a true online simulation.
		#ifdef DEBUG_OUTPUT
		cout<<"Evaluating new baby: "<<endl;
		#endif

		data_record* newrec=new data_record();
		newrec->indiv_number=indiv_counter;
		//evaluate individual, get novelty point
		new_org->noveltypoint = maze_novelty_map(new_org,newrec);
		new_org->noveltypoint->indiv_number = indiv_counter;
		//calculate novelty of new individual
		//archive.evaluate_individual(new_org,pop);
		nov_eval(&archive,new_org,pop,env,true);
		newrec->ToRec[4] = archive.get_threshold();
		newrec->ToRec[5] = archive.get_set_size();
		newrec->ToRec[RECSIZE-2] = new_org->noveltypoint->novelty;

		//add record of new indivdual to storage
		Record.add_new(newrec);
		indiv_counter++;

		//update fittest list
		archive.update_fittest(new_org);
		#ifdef DEBUG_OUTPUT
		cout << "Fitness: " << new_org->fitness << endl;
		cout << "Novelty: " << new_org->noveltypoint->novelty << endl;
		cout << "RFit: " << new_org->noveltypoint->fitness << endl;
		#endif

		//Now we reestimate the baby's species' fitness
		new_org->species->estimate_average();

		//write out the first individual to solve maze
		if(!firstflag && newrec->ToRec[3]>0.0) {
			firstflag=true;
			char filename[100];
			sprintf(filename,"%srtgen_first",output_dir);
			pop->print_to_file_by_species(filename);
			cout << "Maze solved by indiv# " << indiv_counter << " after " << offspring_count << " evaluations" << endl;	
			break;
		}
		//Remove the worst organism                                                                                               
		pop->remove_worst();
	}
	clock_t end_time = clock();
	double elapsed_secs = double(end_time - begin_time) / CLOCKS_PER_SEC;

	//write out run information, archive, and final generation
	cout << "COMPLETED..." << endl;
	
	char fname[150];
	if(!compactLogs){
		sprintf(fname,"%srecord.dat",output_dir);
		Record.serialize(fname);
	}
	sprintf(fname,"%srtarchive.dat",output_dir);
	archive.Serialize(fname);
	
	sprintf(fname,"%sfittest_final",output_dir);
	archive.serialize_fittest(fname);

	sprintf(fname,"%srtgen_final",output_dir);
	pop->print_to_file_by_species(fname);
	
	sprintf(fname,"%spos_final",output_dir);
	pop->print_positions_to_file(fname);
	
	printDiscoveryStats(firstflag,offspring_count,elapsed_secs);
	
	cout << "returning"<< endl;
	
	return 0;
}

//initialize the maze simulation
Environment* mazesimIni(Environment* tocopy,Network *net, vector< vector<float> > &dc) {
    double inputs[20];
    Environment *newenv= new Environment(*tocopy);
	 
	//flush the neural net
	net->flush();
	//update the maze
	newenv->Update();
	//create neural net inputs
	newenv->generate_neural_inputs(inputs);
	//load into neural net
	net->load_sensors(inputs);
	
	//propogate input through net
    for(int i=0;i<10;i++){
		net->activate();
	}
	
	return newenv;
}
  
//execute a timestep of the maze simulation evaluation
double mazesimStep(Environment* newenv,Network *net,vector< vector<float> > &dc) {
	double inputs[20];
	newenv->generate_neural_inputs(inputs);
	net->load_sensors(inputs);
	net->activate();

	//use the net's outputs to change heading and velocity of navigator
	newenv->interpret_outputs(net->outputs[0]->activation,net->outputs[1]->activation);
	//update the environment
	newenv->Update();

	double dist = newenv->distance_to_target();
	if(dist<=1) dist=1;
	double fitness = 5.0/dist; //used for accumulated fitness (obselete)

	return fitness;
}

double mazesim(Network* net, vector< vector<float> > &dc, data_record *record) {
	vector<float> data;
	
	int timesteps=NEAT::timesteps;
	int stepsize=1000;
	
	double fitness=0.0;
	Environment *newenv;
	newenv=mazesimIni(env,net,dc);
	
	//data collection vector initialization
	dc.clear();
	dc.push_back(data);
	dc[0].reserve(param*2+20);
	
	/*ENABLE FOR ADDT'L INFO STUDIES*/
	/*
	if(param>0)
	{
		stepsize=timesteps/param;
	}
	/* */

	for(int i=0;i<timesteps;i++) {
		fitness+=mazesimStep(newenv,net,dc);
		//if taking additional samples, collect during run
		if((timesteps-i-1)%stepsize==0) {
			dc[0].push_back(newenv->hero.location.x);
			dc[0].push_back(newenv->hero.location.y);		
		}
	}

	//calculate fitness of individual as closeness to target
	fitness=300.0 - newenv->distance_to_target();
	if(fitness<0.1){ fitness=0.1; }
	
	//fitness as novelty studies
	//data.push_back(fitness);
	
	float x=newenv->hero.location.x;
	float y=newenv->hero.location.y;
	
	/* ENABLE FOR DISCRETIZATION STUDIES
	if(param>0)
	{
	 long bins=powerof2(param);
	 x=discretize(x,bins,0.0,200.0);
	 y=discretize(y,bins,0.0,200.0);
	}
	*/
	
	if(param<=0) {
		//novelty point is the ending location of the navigator
		dc[0].push_back(x);
		dc[0].push_back(y);
	}
		
	if(record!=NULL) {
		record->ToRec[0]=fitness;
		record->ToRec[1]=newenv->hero.location.x;
		record->ToRec[2]=newenv->hero.location.y;
		record->ToRec[3]=newenv->reachgoal;
		
		/*ADDTL INFO RECORDING */
		if(param>=0) {
			for(int x=0;x<(int)dc[0].size();x++) {
				if((10+x)<RECSIZE)
					record->ToRec[10+x]=dc[0][x];
			}
		}
		/*ADDTL INFO RECORDING */
	}

	delete newenv;
	return fitness;
}

//evaluates an individual and stores the novelty point
noveltyitem* maze_novelty_map(Organism *org,data_record* record) {
	noveltyitem *new_item = new noveltyitem;
	new_item->genotype=new Genome(*org->gnome);
	new_item->phenotype=new Network(*org->net);
	vector< vector<float> > gather;

	double fitness;
	static float highest_fitness=0.0;

	fitness=mazesim(org->net,gather,record);
  	if(fitness>highest_fitness){ 
		highest_fitness=fitness; 
	}
	
	//keep track of highest fitness so hard in record
	if(record!=NULL) {
		/*
		record->ToRec[19]=org->gnome->parent1;
		record->ToRec[18]=org->gnome->parent2;
		record->ToRec[17]=org->gnome->struct_change;
		*/
		record->ToRec[RECSIZE-1]=highest_fitness;
	}

	//push back novelty characterization
	new_item->data.push_back(gather[0]);
	//set fitness (this is 'real' objective-based fitness, not novelty)
	new_item->fitness=fitness;
	return new_item;
}

// ----------------------------------
// SURPRISE SEARCH
// ----------------------------------

//surprise maze navigation run
Population *maze_surprise_realtime(char* outputdir,const char* mazefile,bool useBorders,int par) {
    Population *pop;
    Genome *start_genome;
    char curword[20];
    int id;

	//create new maze environment
	env=new Environment(mazefile,useBorders);
	param=par;
	if(outputdir!=NULL) strcpy(output_dir,outputdir);
		
	//clear a bunch of appendable logs
	char fName[100];
	sprintf(fName,"%sdistance",output_dir);
	std::ofstream *outFile_distance = new std::ofstream(fName,std::ios::out );
	sprintf(fName,"%sfeasible",output_dir);
	std::ofstream *outFile_feasible = new std::ofstream(fName,std::ios::out );
	
	//starter genes file
    ifstream iFile("mazestartgenes",ios::in);
	
    cout<<"START MAZE NAVIGATOR NOVELTY REAL-TIME EVOLUTION VALIDATION"<<endl;

    cout<<"Reading in the start genome"<<endl;
    //Read in the start Genome
    iFile>>curword;
    iFile>>id;
    cout<<"Reading in Genome id "<<id<<endl;
    start_genome=new Genome(id,iFile);
    iFile.close();

    cout<<"Start Genome: "<<start_genome<<endl;

    //Spawn the Population from starter gene
    cout<<"Spawning Population off Genome"<<endl;
    pop=new Population(start_genome,NEAT::pop_size);
      
    cout<<"Verifying Spawned Pop"<<endl;
    pop->verify();
    
    //Start the evolution loop using rtNEAT method calls 
    maze_surprise_realtime_loop(pop);

	cout<<"Experiment done, cleaning up"<<endl;
    //clean up
    delete env;

    delete outFile_distance;
    delete outFile_feasible;

    delete start_genome;
    
	cout<<"Returning Pop"<<endl;
    return pop;
}

//actual rtNEAT loop for novelty maze navigation runs
//int maze_novelty_realtime_loop(Population *pop) {	// BACKUP FOR SANITY
int maze_surprise_realtime_loop(Population *pop) {
	clock_t begin_time = clock();
        
	//NEAT::print_every = 1;
	
	bool firstflag = false; //indicates whether the maze has been solved yet

	vector<Organism*>::iterator curorg;
	vector<Species*>::iterator curspecies;
	vector<Species*>::iterator curspec; //used in printing out debug info                                                         

	vector<Species*> sorted_species;  //Species sorted by max fit org in Species
        
	AbstractSurpriseModel* surprise_model = new KMeansClusteringModel(NEAT::k, NEAT::neigh);
	surprise_model->initLogs(output_dir);
	surprise_model->assignMaze(env);
	
	void (*surprise_eval)(AbstractSurpriseModel*,Organism*,Population*,Environment*);
	surprise_eval = surprise_evaluate_individual;

	logging log;

	data_rec Record; //stores run information

	int count=0;
	int pause;

	//Real-time evolution variables                                                                                             
	int offspring_count;
	Organism *new_org;

	//We try to keep the number of species constant at this number                                                    
	int num_species_target = NEAT::pop_size/NEAT::target_species_size;
  
	//This is where we determine the frequency of compatibility threshold adjustment
	int compat_adjust_frequency = NEAT::pop_size/20;
	if (compat_adjust_frequency < 1){ 
		compat_adjust_frequency = 1; 
	}

	//Initially, we evaluate the whole population                                                                               
	//Evaluate each organism on a test                   
	int indiv_counter=0;  
	for(curorg=(pop->organisms).begin();curorg!=(pop->organisms).end();++curorg) {

		//shouldn't happen                                                                                                        
		if (((*curorg)->gnome)==0) {
			cout<<"ERROR EMPTY GEMOME!"<<endl;
			cin>>pause;
		}

		//evaluate each individual
		(*curorg)->noveltypoint = maze_novelty_map((*curorg));
		(*curorg)->noveltypoint->indiv_number=indiv_counter;
		indiv_counter++;
	}

	//assign fitness scores based on suprise
	surprise_evaluate_population(pop, env, surprise_model, surprise_eval);

	//Get ready for real-time loop
	//Rank all the organisms from best to worst in each species
	pop->rank_within_species();                                                                            

	//Assign each species an average fitness 
	//This average must be kept up-to-date by rtNEAT in order to select species probabailistically for reproduction
	pop->estimate_all_averages();

	cout <<"Entering real time loop..." << endl;

	//Now create offspring one at a time, testing each offspring,                                                               
	// and replacing the worst with the new offspring if its better
	for (offspring_count = 0; offspring_count < NEAT::pop_size*NEAT::generations; offspring_count++) {
		//only continue past generation 1000 if not yet solved
		if(offspring_count>=pop_size*1000 && firstflag){
			break;
		}

		//end of generation
		if(offspring_count % (NEAT::pop_size*1) == 0) {
			log.generation++;
		}

		//write out current generation and fittest individuals
		if( offspring_count % (NEAT::pop_size*NEAT::print_every) == 0 ) {
			cout << "Generation: " << offspring_count/NEAT::pop_size << endl;
			char fname[100];

			sprintf(fname,"%sfittest_%d",output_dir,offspring_count/NEAT::pop_size);
			log.serialize_fittest(fname);

			//cout << "saving fittest to " << fname << endl;
			
			if(!compactLogs){
				sprintf(fname,"%srtgen_%d",output_dir,offspring_count/NEAT::pop_size);
				pop->print_to_file_by_species(fname);
			}
			sprintf(fname,"%spos_%d",output_dir,offspring_count/NEAT::pop_size);
			pop->print_positions_to_file(fname);
		}
		if(offspring_count % (NEAT::pop_size) == 0 ) {	// every generation
			surprise_model->updateValues(pop); //update surprise model
            surprise_evaluate_population(pop, env, surprise_model, surprise_eval);
                        
			char fname[100];
			sprintf(fname,"%sdistance",output_dir);
			pop->print_distance_to_progress_file(env->end,fname);
			sprintf(fname,"%sfeasible",output_dir);
			pop->print_feasible_to_progress_file(env,fname);
		}
		
		//Every pop_size reproductions, adjust the compat_thresh to better match the num_species_targer
		//and reassign the population to new species                                              
		if (offspring_count % compat_adjust_frequency == 0) {
			count++;
			#ifdef DEBUG_OUTPUT
			cout << "Adjusting..." << endl;
			#endif

			//update fittest individual list		
			log.update_fittest(pop);

			int num_species = pop->species.size();
             //cout << "NUM SPECIES " << num_species << endl;
			double compat_mod = 0.1;  //Modify compat thresh to control speciation                                                     

			// This tinkers with the compatibility threshold 
			if (num_species < num_species_target) {
				NEAT::compat_threshold -= compat_mod;
			}
			else if (num_species > num_species_target){
				NEAT::compat_threshold += compat_mod;
			}
			
			if (NEAT::compat_threshold < 0.3){
				NEAT::compat_threshold = 0.3;
			}
			
			#ifdef DEBUG_OUTPUT
			cout<<"compat_thresh = "<<NEAT::compat_threshold<<endl;
			#endif

			//Go through entire population, reassigning organisms to new species                                                  
			for (curorg = (pop->organisms).begin(); curorg != pop->organisms.end(); ++curorg) {
				pop->reassign_species(*curorg);
			}
		}

		//For printing only
		#ifdef DEBUG_OUTPUT
		for(curspec=(pop->species).begin();curspec!=(pop->species).end();curspec++) {
			cout<<"Species "<<(*curspec)->id<<" size"<<(*curspec)->organisms.size()<<" average= "<<(*curspec)->average_est<<endl;
		}

		cout<<"Pop size: "<<pop->organisms.size()<<endl;
		#endif

		//Here we call two rtNEAT calls: 
		//1) choose_parent_species() decides which species should produce the next offspring
		//2) reproduce_one(...) creates a single offspring fromt the chosen species
		new_org = (pop->choose_parent_species())->reproduce_one(offspring_count,pop,pop->species);

		//Now we evaluate the new individual
		//Note that in a true real-time simulation, evaluation would be happening to all individuals at all times.
		//That is, this call would not appear here in a true online simulation.
		#ifdef DEBUG_OUTPUT
		cout<<"Evaluating new baby: "<<endl;
		#endif

		data_record* newrec = new data_record();
		newrec->indiv_number = indiv_counter;
		
		//evaluate individual
		new_org->noveltypoint = maze_novelty_map(new_org, newrec);
		new_org->noveltypoint->indiv_number = indiv_counter;
		
		//calculate surprise of the new individual
		surprise_eval(surprise_model, new_org, pop, env);

		newrec->ToRec[4] = 0;
		newrec->ToRec[5] = 0;
		newrec->ToRec[RECSIZE-2] = 0;

		//add record of new individual to storage
		Record.add_new(newrec);
		indiv_counter++;

		//update fittest list
		log.update_fittest(new_org);
		#ifdef DEBUG_OUTPUT
		cout << "Fitness: " << new_org->fitness << endl;
		cout << "Novelty: " << new_org->noveltypoint->novelty << endl;
		cout << "RFit: " << new_org->noveltypoint->fitness << endl;
		#endif

		//Now we reestimate the baby's species' fitness
		new_org->species->estimate_average();

		//write out the first individual to solve maze
		if(!firstflag && newrec->ToRec[3]>0.0) {
			firstflag=true;
			char filename[100];
			sprintf(filename,"%srtgen_first",output_dir);
			pop->print_to_file_by_species(filename);
			cout << "Maze solved by indiv# " << indiv_counter << " after " << offspring_count << " evaluations" << endl;	
			break;
		}
		//Remove the worst organism                                                                                               
		pop->remove_worst();
	}

	clock_t end_time = clock();
	double elapsed_secs = double(end_time - begin_time) / CLOCKS_PER_SEC;

	//write out run information, archive, and final generation
	cout << "COMPLETED..." << endl;
	
	char fname[150];
	if(!compactLogs){
		sprintf(fname,"%srecord.dat",output_dir);
		Record.serialize(fname);
	}
	
	sprintf(fname,"%sfittest_final",output_dir);
	log.serialize_fittest(fname);

	sprintf(fname,"%srtgen_final",output_dir);
	pop->print_to_file_by_species(fname);
	
	sprintf(fname,"%spos_final",output_dir);
	pop->print_positions_to_file(fname);
	
	printDiscoveryStats(firstflag,offspring_count,elapsed_secs);
        
    cout << "surprise part cleaning" << endl;
        
    delete surprise_model;
	
	cout << "returning"<< endl;
	
	return 0;
}
